<cfoutput>
    
    <div class="container-swiper-vertical">

        <cfif args.useMobileFolder>
            <div class="container container-swiper-vertical p-0 m-0 d-md-none">
                <!--- only smaller screens, depends on bootstrap --->
                <div class="swiper mySwiper-#args.swiperMobileID#">
                    <div class="swiper-wrapper">
                        <cfloop query="args.swiperImagesMobile">
                            <div class="swiper-slide">
                                #renderAsset(assetId=id, args={ derivative="swiperMobile" })#
                            </div>
                        </cfloop>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </cfif>

        <div class="container container-swiper-vertical <cfif args.useMobileFolder>d-none d-md-block</cfif>">
            <!--- only larger screens, depends on bootstrap --->   
            <div class="swiper mySwiper-#args.swiperID#">
                <div class="swiper-wrapper">
                    <cfloop query="args.swiperImages">
                        <div class="swiper-slide">
                            #renderAsset(assetId=id, args={ derivative="swiperFull" })#
                        </div>
                    </cfloop>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
            
    </div>
</cfoutput>
