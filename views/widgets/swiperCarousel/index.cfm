<cfscript>
	event.include( "css-swiper-default" ).include( "css-swiper-custom" ).include( "js-swiper");
</cfscript>

<cfoutput>

    #args.sCssBlock#
    
    <div class="container p-0">

        <div class="container px-0 text-center">
                
            <cfif val(args.showSwiperTitle) eq 1>
                <h2>#args.swiperTitle#</h2>
            </cfif>

            <cfif val(args.showSwiperDescription) eq 1>
                <h3>#args.swiperDescription#</h3>
            </cfif>
        
        </div>      

        <cfif args.swiperDirection eq "vertical">
            <cfinclude template="_vertical.cfm">
        <cfelse>
            <cfinclude template="_horizontal.cfm">    
        </cfif>

    </div>    

    <script>
        window.addEventListener('load', function() {
            
            var swiper = new Swiper(".mySwiper-#args.swiperID#", {

            direction: "#args.swiperDirection#",
            effect: "#args.swiperEffect#", 

            <cfif args.swiperAutoplay eq 1>
                autoplay: {
                    delay: #args.swiperAutoplayDelay#,
                },
            </cfif>            
            
            <cfif args.swiperEffect eq "creative">
                creativeEffect: {
                    prev: {
                    // will set `translateZ(-400px)` on previous slides
                    translate: [0, 0, -400],
                    },
                    next: {
                    // will set `translateX(100%)` on next slides
                    translate: ['100%', 0, 0],
                    },
                },            
            </cfif>

            <cfif args.swiperDirection eq "horizontal">
                <!--- horizontal --->
                autoHeight: #args.swiperAutoheight#,
                pagination: {
                    el: ".swiper-pagination",
                    type: "progressbar",
                },
                navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },                    
            <cfelse>
                <!--- vertical --->
                pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },                     
            </cfif>
            speed: #args.swiperSpeed#,
            loop: #args.swiperLoop#,
            });

        <cfif args.useMobileFolder>

            var swiper = new Swiper(".mySwiper-#args.swiperMobileID#", {

            direction: "#args.swiperDirection#",
            effect: "#args.swiperEffect#", 

            <cfif args.swiperEffect eq "creative">
                creativeEffect: {
                    prev: {
                    // will set `translateZ(-400px)` on previous slides
                    translate: [0, 0, -400],
                    },
                    next: {
                    // will set `translateX(100%)` on next slides
                    translate: ['100%', 0, 0],
                    },
                },            
            </cfif>

            <cfif args.swiperDirection eq "horizontal">
                <!--- horizontal --->
                autoHeight: #args.swiperAutoheight#,
                pagination: {
                    el: ".swiper-pagination",
                    type: "progressbar",
                },
                navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },                    
            <cfelse>
                <!--- vertical --->
                pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },                     
            </cfif>
            speed: #args.swiperSpeed#,
            loop: #args.swiperLoop#,
            });

        </cfif>

        });     
    </script>         

</cfoutput>
