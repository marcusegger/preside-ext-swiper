component {

    property name="presideObjectService" inject="presideObjectService";        

    private function index( event, rc, prc, args={} ) {

        args.useMobileFolder = ( len(args.swiperFolderMobile) gt 0 ) ? true : false;

        args.swiperImages = presideObjectService.selectData(
                objectName   = "asset",
                filter = { asset_folder = args.swiperFolder, is_trashed = 0 }
        );

        if (args.useMobileFolder) {
            args.swiperImagesMobile = presideObjectService.selectData(
                objectName   = "asset",
                filter = { asset_folder = args.swiperFolderMobile, is_trashed = 0 }
            );
            args.swiperMobileID = hash(args.swiperFolderMobile);            
        }

        args.swiperID = hash(args.swiperFolder & rand());

        savecontent variable="args.sCssBlock" {
            writeOutput("
            <style>
                .mySwiper-#args.swiperID# .swiper-button-prev, .mySwiper-#args.swiperID# .swiper-button-next { color: #args.swiperNavColor#; }
                .mySwiper-#args.swiperID# .swiper-pagination-progressbar .swiper-pagination-progressbar-fill { background: #args.swiperNavColor#; }
                .mySwiper-#args.swiperID# .swiper-pagination-bullet { background: #args.swiperNavColor#; }
                .container-swiper-vertical { height: 480px}
                .mySwiper-#args.swiperID# .swiper-pagination-bullet { width: #args.swiperVerticalBulletSize#;height: #args.swiperVerticalBulletSize#;}
            </style>");
        }        
        
        return renderView( view="widgets/swiperCarousel/index", args=args ); 

    }
}
