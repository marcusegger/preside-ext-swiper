# Preside Extension for a Swiper based Slider

This extension allows you to place a slider element as a widget. We use the awesome [Swiper](https://swiperjs.com/) library.

## Details

* support for all swiper effects and animation speed

* custom colors for navigation / progress bar

* support for autoplay and autoplay speed

* support for different mobile slides (if you want)

* custom navigation size

* support for horizontal and vertical sliders

* support for looping

* support for title/description above your slider widget

* support for autoheight

