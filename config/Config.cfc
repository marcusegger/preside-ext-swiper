component {

	/**
	 * This extension Config.cfc has been scaffolded by the Preside
	 * Scaffolding service.
	 *
	 * Override or append to core Preside/Coldbox settings here.
	 *
	 */
	public void function configure( required struct config ) {
		// core settings that will effect Preside
		var settings            = arguments.config.settings            ?: {};

		// other ColdBox settings
		var coldbox             = arguments.config.coldbox             ?: {};
		var i18n                = arguments.config.i18n                ?: {};
		var interceptors        = arguments.config.interceptors        ?: {};
		var interceptorSettings = arguments.config.interceptorSettings ?: {};
		var cacheBox            = arguments.config.cacheBox            ?: {};
		var wirebox             = arguments.config.wirebox             ?: {};
		var logbox              = arguments.config.logbox              ?: {};
		var environments        = arguments.config.environments        ?: {};

		settings.enum.swiper_speed = [ "100","200","300","400","500","600","700","800","900","1000", "2000" ];
		settings.enum.swiper_effect = [ "slide","fade","cube","coverflow","flip","creative","cards" ];
		settings.enum.swiper_direction = [ "horizontal","vertical" ];
		settings.enum.swiper_loop = [ "true","false" ];
		settings.enum.swiper_autoheight = [ "true","false" ];
		settings.enum.swiper_vertical_bullets = [ "0.5rem","0.6rem","0.7rem","0.8rem","0.9rem","1rem","2rem" ];
		settings.enum.swiper_delay = [ "1000", "2000", "3000", "4000", "5000", "6000", "7000", "8000", "9000", "10000" ];

		settings.assetmanager.derivatives.swiperThumb = {
			permissions     = "inherit"
		  , transformations = [ { method="resize", args={ width=300, height=225, maintainAspectRatio=true, useCropHint=true } } ]
	  	};
	  
		settings.assetmanager.derivatives.swiperFull = {
			permissions     = "inherit"
		, transformations = [ { method="resize", args={ width=1280, height=480 } } ]
		};
		
		settings.assetmanager.derivatives.swiperMobile = {
			permissions     = "inherit"
		, transformations = [ { method="resize", args={ width=360 } } ]
		};		

	}
}