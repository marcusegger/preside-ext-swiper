component output=false {

        public void function configure( bundle ) output=false {
        
                // swiper
                bundle.addAsset( id="js-swiper", path="/js/ext-preside-swiper-slider-swiper-bundle.min.js" );
                bundle.addAsset( id="css-swiper-default", path="/css/ext-preside-swiper-slider-swiper-bundle.min.css" );
                bundle.addAsset( id="css-swiper-custom", path="/css/ext-preside-swiper-slider-swiper-custom.min.css" );
	}
}